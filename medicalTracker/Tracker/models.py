from django.db import models
from django.db.models import Model
from django.urls import reverse

ALL_STATES= (("Alabama","Alabama"),("Alaska","Alaska"),("Arizona","Arizona"),("Arkansas","Arkansas"),
("California","California"),("Colorado","Colorado"),("Connecticut","Connecticut"),("Delaware","Delaware"),
("Florida","Florida"),("Georgia","Georgia"),("Hawaii","Hawaii"),("Idaho","Idaho"),("Illinois","Illinois"),
("Indiana","Indiana"),("Iowa","Iowa"),("Kansas","Kansas"),("Kentucky","Kentucky"),("Louisiana","Louisiana"),
("Maine","Maine"),("Maryland","Maryland"),("Massachusetts","Massachusetts"),("Michigan","Michigan"),
("Minnesota","Minnesota"),("Mississippi","Mississippi"),("Missouri","Missouri"),("Montana","Montana"),
("Nebraska","Nebraska"),("Nevada","Nevada"),("New Hampshire","New Hampshire"),("New Jersey","New Jersey"),
("New Mexico","New Mexico"),("New York","New York"),("North Carolina","North Carolina"),("North Dakota","North Dakota"),
("Ohio","Ohio"),("Oklahoma","Oklahoma"),("Oregon","Oregon"),("Pennsylvania","Pennsylvania"),
("Rhode Island","Rhode Island"),("South Carolina","South Carolina"),("South Dakota","South Dakota"),
("Tennessee","Tennessee"),("Texas","Texas"),("Utah","Utah"),("Vermont","Vermont"),("Virginia","Virginia"),
("Washington","Washington"),("West Virginia","West Virginia"),("Wisconsin","Wisconsin"),("Wyoming","Wyoming"))

Provider_Options = ("Primary Care", "Primary Care"), ("Oncology", "Oncology"), ("Imaging", "Imaging")
# Create your models here.
class Provider(models.Model):
    name = models.CharField(max_length=200)
    specialty = models.CharField(max_length=200, choices=Provider_Options)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=25, choices=ALL_STATES, default="Montana")
    phone = models.CharField(max_length=16)
    zip_code = models.IntegerField(default=False)

class Medication(models.Model):
    name = models.CharField(max_length=200)
    dose = models.CharField(max_length=200)
    frequency = models.CharField(max_length=200)

class Visit(models.Model):
    provider = models.ForeignKey(
        "Provider", 
        related_name="visits", 
        on_delete=models.PROTECT,
    )
    date = models.DateTimeField(auto_now_add=True)
    notes = models.CharField(max_length=200)