from django.contrib import admin

from Tracker.models import Provider, Medication, Visit

# Register your models here.
class ProviderAdmin(admin.ModelAdmin):
    pass

class MedicationAdmin(admin.ModelAdmin):
    pass

class VisitAdmin(admin.ModelAdmin):
    pass

admin.site.register(Provider, ProviderAdmin)
admin.site.register(Medication,MedicationAdmin)
admin.site.register(Visit,VisitAdmin)