# Generated by Django 4.1.5 on 2023-01-25 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("Tracker", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="provider",
            name="zip_code",
            field=models.IntegerField(default=False),
        ),
    ]
